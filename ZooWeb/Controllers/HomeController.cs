﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ZooWeb.Controllers
{
    public class HomeController : Controller
    {

        ZooEntities storeDB = new ZooEntities();

        public ActionResult Index()
        {
            //var db = DB_Manager_Field.Open();
            //Logic.Init(storeDB);
            ViewBag.Message = "home controller index.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "home controller about.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "home controller contact page.";

            return View();
        }
    }
}
