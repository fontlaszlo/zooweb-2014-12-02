﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZooWeb.Models;
using System.Text.RegularExpressions;

namespace ZooWeb.Controllers
{
    public class AddNewAnimalController : Controller
    {
        ZooEntities storeDB = new ZooEntities();

        public ActionResult Index()
        {
            List<string> list = new List<string>();
            list = DB_Manager_Animal.GetSpeciesNameList(storeDB);

            return View(list);
        }

        public ActionResult FillAnimalData(string speciesname)
        {            
            Animal animal = new Animal("GiveAName", speciesname);

            return View(animal);
        }


        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult DispAnimalDataSelectField()
        {
            string speciesName = Request.Form["SpeciesName000"];
            string firstName = Request.Form["FirstName000"];

            Animal animal = new Animal(firstName, speciesName); //for listbox only

            List<string> list = new List<string>();

            list = Logic.CreateListOfFieldsIdStringsToPlace(animal, storeDB);
             
            List<SelectListItem> selList = new List<SelectListItem>();

            //selList.Add(new SelectListItem { Text = "Week 1", Value = "Week1" });            
            int i = 0;
            foreach (string item in list)
	        {
                selList.Add(new SelectListItem { Text = list.ElementAt(i), Value = list.ElementAt(i) });
                i++;
	        }
            
            ViewBag.List = selList;
            ViewBag.Animal = animal;
            return View();       
        }

        [HttpPost]
        public ActionResult SaveAnimal(string SelId)
        {
            string speciesName = Request.Form["SpeciesName000"];
            string firstName = Request.Form["FirstName000"];

            Animal animal = new Animal(firstName, speciesName); //for listbox only
            
            ViewBag.Selstr = SelId;
            ViewBag.CreateFieldMsg = "";
            

            if(SelId==null)
            {
                ViewBag.CreateFieldMsg = "Pleasse create a place for the animal.";
            }
            else
            {

                string newString = Regex.Replace(SelId, "[^.0-9]", "");
                int num = int.Parse(newString);
                animal.SetLivingFieldNb(num);
                storeDB.Animals.Add(animal);


                Field field = DB_Manager_Field.GetAField(num, storeDB);
                field.AnimalsInOnePlace.Add(animal);

                storeDB.SaveChanges();
            }
            
            
            //return View(); 
            return RedirectToAction("Index");

        }        
        
    }
}
