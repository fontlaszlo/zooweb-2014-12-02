﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooWeb
{
    public class Gaselle : Animal
    {
        public double SpeedKmh { get; protected set; }

        public Gaselle(string aAnimalName, double speedKmh)
            : base(aAnimalName, "Antilope")
        {
            SpeedKmh = speedKmh;
        }


        public Gaselle()
            : this("",0.0)
        {

        }

    }
}