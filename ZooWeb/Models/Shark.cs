﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;


namespace ZooWeb
{
    public class Shark : Animal, ISwimm
    {
        public int CountOfTeeth { get; protected set; }        

        public Shark(string aAnimalName)
            : base(aAnimalName, "Shark")
        {
            CountOfTeeth = 199;
        }

        public Shark():this("")
        {

        }

        void ISwimm.Swimm()
        {
            Console.WriteLine("Shark is a good swimmer");
        }


    }
}
