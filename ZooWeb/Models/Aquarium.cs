﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooWeb
{
    public class Aquarium : Field
    {
        public int HeightCm { get; protected set; }

        public Aquarium(int maxNbOfAnimals, int heightCm)
            : base(maxNbOfAnimals, "Aquarium")
        {
            HeightCm = heightCm;
        }

        public Aquarium()
            : base(0, "Aquarium")
        {
            HeightCm = 0;
        }

    }

}
