﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooWeb
{
    public class Antilope: Animal
    {
        public double SpeedKmh { get; protected set; }        

        public Antilope(string aAnimalName, double speedKmh)
            : base(aAnimalName, "Antilope")
        {
            SpeedKmh = speedKmh;
        }


        public Antilope()
            : this("", 0.0)
        {

        }

    }
}