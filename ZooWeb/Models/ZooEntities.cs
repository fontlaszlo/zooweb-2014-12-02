﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ZooWeb
{
    // ZooEntities = old ZooEntities
    public class ZooEntities : DbContext
    {
        public DbSet<Animal> Animals { get; set; }
        public DbSet<Field> Fields { get; set; }
    }
}
