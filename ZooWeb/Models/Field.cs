﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace ZooWeb
{
    
    public class Field
    {

        [Key]
        public int FieldId { get; protected set; }
        public string PlaceType { get; protected set; }

        public int MaxCountOfAnimals { get; protected set; }
        public int WidthCm { get; protected set; }
        public int LengthCm { get; protected set; }
           
        public virtual List<Animal> AnimalsInOnePlace { get; protected set; } 

        public Field():this(2,"OpenField")
        {

        }

        public Field(int aMaxNbOfAnimals, string aPlaceType)
        {
            if (aMaxNbOfAnimals < 1 || aMaxNbOfAnimals > 100)
            {
                Console.WriteLine("Error 10: Max Nb Of Animals is out of range.");
                return;
            }
            WidthCm = 100 * 100;
            LengthCm = 200 * 100;
            PlaceType = aPlaceType;
            MaxCountOfAnimals = aMaxNbOfAnimals;
            AnimalsInOnePlace = new List<Animal>();

            //Console.WriteLine("New " + aPlaceType + "  created. Field ID " + FieldId);
        }
        
        public bool IsSuitable(Animal aAnimal, string aPlaceType)
        {
            if (PlaceType == aPlaceType)
            {
                if (MaxCountOfAnimals > AnimalsInOnePlace.Count())
                {
                   return true;
                }
            }

            return false;
        }

        public void FieldInventory()
        {
            Console.WriteLine(this.PlaceType + " " + this.AnimalsInOnePlace.Count() + "/" + this.MaxCountOfAnimals + " id: " + this.FieldId);
            foreach (var item in AnimalsInOnePlace)
            {
                Console.WriteLine(" " + item.FirstName + " " + " (" + item.SpeciesName + ") id: " + item.AnimalId + " ");
            }
        }

        public int GetLengthCm()
        {
            return LengthCm;
        }

        public int GetWidthCm()
        {
            return WidthCm;
        }

        public bool SetLengthCm(int aLengthCm)
        {
            LengthCm = aLengthCm;
            return true;
        }

        public bool SetWidthCm(int aWidthCm)
        {
            WidthCm = aWidthCm;
            return true;
        }

        public bool SetPlaceType( string placeType)
        {
            PlaceType = placeType;
            return true;
        }

        public bool SetMaxCountOfAnimals(int maxCountOfAnimals)
        {
            MaxCountOfAnimals = maxCountOfAnimals;
            return true;
        }

        public Field Get()
        {
            return this;
        }

        public void SetAnimalsInOnePlace  (List<Animal> animalsInOnePlace )
        {
            AnimalsInOnePlace = animalsInOnePlace;
        }
    }
    
}
