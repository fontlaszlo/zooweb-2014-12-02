﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ZooWeb
{
    class DB_Manager_Field
    {
        public static void AddField(int maxCountOfAnimals, string aFieldType, ZooEntities db)
        {
           
            var field = new Field(maxCountOfAnimals, aFieldType);
            db.Fields.Add(field);
            //db.SaveChanges();
        }


        public static Field GetAField(int id, ZooEntities db)
        {
            var query = from b in db.Fields
                        where b.FieldId == id
                        select b;
            return (Field)query.First() ;
        }

        public static ZooEntities Open()
        {
            var db = new ZooEntities();
            return db;
        }


        public static void WriteOutFieldSet(ZooEntities db)
        {
            var query = from b in db.Fields
                        select b;

            Console.WriteLine("All Fields in the database:");
            foreach (var item in query)
            {
                Console.WriteLine("field type: " + item.PlaceType + " Max Number of Animals: " + item.MaxCountOfAnimals + " ID: " + item.FieldId.ToString());
            }
        }


        public static void DeleteDBSet_Field(ZooEntities db)
        {
            foreach (var entity in db.Fields)
            {
                db.Fields.Remove(entity);
            }
               
            //db.SaveChanges();

            //Console.WriteLine("DeleteFieldSet");
        }

        public static int GetNbOfFields(ZooEntities db)
        {
            Console.WriteLine("GetNbOfFields");
            return db.Fields.Count();
        }

    }
}
