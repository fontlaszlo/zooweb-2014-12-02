﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooWeb
{
    public class MonkeyHouse : Field
    {
        public int CountOfClimbingBar { get; protected set; }

        public MonkeyHouse(int maxNbOfAnimals, int countOfClimbingBar)
            : base(maxNbOfAnimals, "MonkeyHouse")
        {
            CountOfClimbingBar = countOfClimbingBar;
        }

        public MonkeyHouse():this(0,0)
        {

        }

        public void SetNbOfClimbingBar(int aNbOfClimbingBar)
        {
            CountOfClimbingBar = aNbOfClimbingBar;
        }
    }

}
