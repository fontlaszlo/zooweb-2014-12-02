﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace ZooWeb
{
    public class Animal : IEat
    {
        [Key]
        public int AnimalId { get; private set; }
        public string SpeciesName { get; set; }
        public bool IsCarnivorous { get; protected set; }

        public Nullable<DateTime> BirthDay { get; protected set; }
        public string FirstName { get; protected set; }
        public string LivingFieldType { get; protected set; }
        public int LivingFieldNb { get; protected set; }

        public Animal(string firstName, string speciesName)
        {
            BirthDay = Convert.ToDateTime("1999,01,01");
            FirstName = firstName;
            SpeciesName = speciesName;
            IsCarnivorous = Logic.IsCarnivorours(speciesName);
            LivingFieldType = Logic.GetSuitableFieldType(speciesName);
        }

        public Animal():this("","" )
        {
        }

        public string GetSpeciesName()
        {
            return SpeciesName;
        }

        public bool IsCarnevorous()
        {
            return IsCarnivorous;
        }


       

        public void SetLivingFieldNb(int livingFieldNb)
        {
            LivingFieldNb = livingFieldNb;
        }

        public int GetAnimalID()
        {
            return AnimalId;
        }

        public string GetAnimalName()
        {
            return FirstName;
        }

        public bool SetBirthDay(string aStrBirthday)
        {
            BirthDay = Convert.ToDateTime(aStrBirthday);
            return true;
        }

        public DateTime GetBirthday()
        {
            return (DateTime)BirthDay;
        }


        void IEat.Eating()
        {
            Console.WriteLine("Animal eats twice a day");
        }
    }

}
