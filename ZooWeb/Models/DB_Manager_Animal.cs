﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooWeb
{
    public static class DB_Manager_Animal
    {
        public static void AddAnimal(string firstName, string speciesName, ZooEntities db)
        {           
            var animal = new Animal(firstName, speciesName);
            db.Animals.Add(animal);
            //db.SaveChanges();
            
        }

        public static void AddAnimal(Animal animal, ZooEntities db)
        {
           
            db.Animals.Add(animal);
            //db.SaveChanges();
            
        }

        public static List<string> GetSpeciesNameList(ZooEntities db)
        {

            List<string> list = new List<string>();

            var query = from b in db.Animals
                        orderby b.SpeciesName
                        select b;

            int counter = 0;
            string LastSpNamestr = "";

            foreach (var item in query)
            {
                if (item.SpeciesName != LastSpNamestr)
                {
                    counter++;
                    list.Add(/*counter.ToString() + " - " +*/ item.SpeciesName);

                }

                LastSpNamestr = item.SpeciesName;
            }

            return list;
        }

        public static void WriteOutDeailedSpeciesSet(ZooEntities db)
        {            
            var query = from b in db.Animals
                        orderby b.SpeciesName
                        select b;

            //Console.WriteLine("WriteOutSpeciesSet");
            int counter = 0;
            foreach (var item in query)
            {
                counter++;
                Console.WriteLine(counter.ToString() + ": " + item.SpeciesName + " Carnivorous: " + item.IsCarnevorous());
            }            
        }

        public static Animal GetASpecies(string aSpeciesName, ZooEntities db)
        {
            
            //return (Species)(db.DBSet_Species.Where(p => p.SpeciesName == aSpeciesName)).First();

            //var query = from b in db.DBSet_Species
            //            where b.SpeciesName == aSpeciesName
            //            select b;

            //Species sp = (Species)query;


            var query = from b in db.Animals
                        orderby b.SpeciesName
                        select b;

            //Console.WriteLine("GetASpecies");
            foreach (var item in query)
            {
                if( item.SpeciesName == aSpeciesName )
                {
                    return item;
                }
            }

            Console.WriteLine("Error 2: No GetASpecies found!");
            return null;


            //return (Species)query;
            
        }

        public static Animal GetASpeciesBySpeciesAlphabNameListNb(int aAlphabNameListNb, ZooEntities db)
        {            
            var query = from b in db.Animals
                        orderby b.SpeciesName
                        select b;

            //Console.WriteLine("GetASpeciesBySpeciesAlphabNameListNb");
            int counter = 0;

            string LastSpNamestr = "";
            foreach (var item in query)
            {
                if( item.SpeciesName != LastSpNamestr )
                {
                    counter++;
                    if (counter == aAlphabNameListNb)
                    {
                        return item;
                    }
                }
                    
                LastSpNamestr = item.SpeciesName;
            }

            Console.WriteLine("Error 3: No GetASpeciesBySpeciesAlphabNameListNb found!");
            return null;
        }

        public static void DeleteDBSet_Animal(ZooEntities db)
        {
            
            foreach (var entity in db.Animals)
                db.Animals.Remove(entity);
            //db.SaveChanges();

            //Console.WriteLine("DeleteSpeciesSet");
           
        }

        public static int GetNbOfSpecies(ZooEntities db)
        {
            
            //Console.WriteLine("GetNbOfSpecies");
            return db.Animals.Count();
            
        }
    }
}
