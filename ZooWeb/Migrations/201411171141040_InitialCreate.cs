namespace ZooWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Animals",
                c => new
                    {
                        AnimalId = c.Int(nullable: false, identity: true),
                        SpeciesName = c.String(),
                        IsCarnivorous = c.Boolean(nullable: false),
                        BirthDay = c.DateTime(),
                        FirstName = c.String(),
                        LivingFieldType = c.String(),
                        LivingFieldNb = c.Int(nullable: false),
                        SpeedKmh = c.Double(),
                        SpeedKmh1 = c.Double(),
                        CountOfTeeth = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Field_FieldId = c.Int(),
                    })
                .PrimaryKey(t => t.AnimalId)
                .ForeignKey("dbo.Fields", t => t.Field_FieldId)
                .Index(t => t.Field_FieldId);
            
            CreateTable(
                "dbo.Fields",
                c => new
                    {
                        FieldId = c.Int(nullable: false, identity: true),
                        PlaceType = c.String(),
                        MaxCountOfAnimals = c.Int(nullable: false),
                        WidthCm = c.Int(nullable: false),
                        LengthCm = c.Int(nullable: false),
                        HeightCm = c.Int(),
                        CountOfClimbingBar = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.FieldId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Animals", new[] { "Field_FieldId" });
            DropForeignKey("dbo.Animals", "Field_FieldId", "dbo.Fields");
            DropTable("dbo.Fields");
            DropTable("dbo.Animals");
        }
    }
}
