﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using ZooWeb.Models;


namespace ZooWeb
{
    public static class Logic
    {  
        public static bool TryAccomodate(Animal aAnimal, ZooEntities db)
        {
            var query = from b in db.Fields
                        orderby b.FieldId
                        select b;

            foreach (var field in query)            
            {
                if ( field.IsSuitable(aAnimal, Logic.GetSuitableFieldType(aAnimal.SpeciesName)) )
                {
                    return true;
                }
            }

            Console.WriteLine("NO place for " + aAnimal.GetSpeciesName() + ". Please create a new " + Logic.GetSuitableFieldType(aAnimal.SpeciesName) +".");
            
            return false;
        }

       
        public static void Inventory(ZooEntities db)
        {

            Console.WriteLine("\nInventory");

            foreach (Field field in db.Fields)
            {
                field.FieldInventory();
            }
        }

        public static void Init(ZooEntities db)
        {
            DB_Manager_Field.DeleteDBSet_Field(db);
            DB_Manager_Animal.DeleteDBSet_Animal(db);

            db.SaveChanges();

            DB_Manager_Animal.AddAnimal("For the Menu only!", "Lion", db);
            DB_Manager_Animal.AddAnimal("For the Menu only!", "Antilope", db);
            DB_Manager_Animal.AddAnimal("For the Menu only!", "Chimpanzee", db);
            DB_Manager_Animal.AddAnimal("For the Menu only!", "Gaselle", db);
            DB_Manager_Animal.AddAnimal("For the Menu only!", "Shark", db);
            //DB_Manager_Species.WriteOutSpeciesListOnly();

            DB_Manager_Field.AddField(2, "Cage", db);
            DB_Manager_Field.AddField(3, "Cage", db);
            DB_Manager_Field.AddField(2, "Aquarium", db);
            DB_Manager_Field.AddField(2, "MonkeyHouse", db);
            DB_Manager_Field.AddField(2, "OpenField", db);
            //DB_Manager_Field.WriteOutFieldSet();

            db.SaveChanges();
            Animal animal = new Animal("Leo", "Lion");
            bool isOK = TryAccomodate(animal, db);


            db.SaveChanges();
            ;
        }

        public static bool  IsCarnivorours(string SpeciesName)
        {
            if (SpeciesName == "Antilope")
            {
                return false;
            }

            if (SpeciesName == "Gaselle")
            {
                return false;
            }

            return true;
        }

        public static string GetSuitableFieldType(string SpeciesName)
        {
            if (SpeciesName == "Shark")
            {
                return "Aquarium";
            }

            if (SpeciesName == "Chimpanzee")
            {
                return "MonkeyHouse";
            }

            if (SpeciesName == "Lion")
            {
                return "Cage";
            }

            return "OpenField";
        }

        public static List<string> CreateListOfFieldsIdStringsToPlace(Animal animal, ZooEntities db)
        {
            List<string> list = new List<string>();


            var query = from b in db.Fields
                        orderby b.FieldId
                        select b;

            foreach (var field in query)
            {
                if (field.IsSuitable(animal, animal.LivingFieldType))
                {
                    list.Add(field.PlaceType + " " + field.FieldId);
                }
            }

            return list;
        }

    }
}
